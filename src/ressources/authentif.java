package ressources;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.persistence.Entity;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import smartbook.entities.Utilisateur;
import smartbook.service.GestionUtilisateurRemote;

@Entity
@Path("users")
public class authentif {
	@EJB
	private GestionUtilisateurRemote  gestion;
	ArrayList <Utilisateur> list =new  ArrayList<Utilisateur>();
	//Ajout user
	@POST
	@Consumes({MediaType.APPLICATION_JSON })
	@Path(value="inscrit")
	public Response Ajout(Utilisateur user) {
		gestion.addUtilisateur(user);
		return Response.status(Status.CREATED).entity("Ajout Utilisateur en JSON").build();
	}
	//authentifier form
	@GET
	@Consumes("application/x-www-form-urlencoded")
	@Path("loginf")
	public String loginf (@FormParam("email")String mail,@FormParam("password")String pwd) {
		if (mail.isEmpty()||pwd.isEmpty())
		{
			return "failed";
		}else
		{
			return gestion.loginUtilisateur(mail, pwd)+" login avec sucess form";
		}
	}
}





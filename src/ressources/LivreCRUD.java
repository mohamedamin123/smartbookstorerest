package ressources;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.Entity;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import smartbook.entities.Utilisateur;
import smartbook.entities.livre;
import smartbook.service.GestionLivreRemote;
@Entity
@Path("livrecrud")
public class LivreCRUD {
	@EJB
	private GestionLivreRemote  gestion;
	ArrayList <livre> list =new  ArrayList<livre>();
	
	//Ajout livre
		@POST
		@Consumes({MediaType.APPLICATION_JSON })
		@Path(value="add")
		public Response Ajout(livre livre) {
			gestion.addlivre(livre);
			return Response.status(Status.CREATED).entity("Ajout livre en JSON").build();
		}
		//find By numSerie
		@GET
		@Produces({MediaType.APPLICATION_JSON})
		@Path("bynum/{numSerie}")
		public livre getlivre(@PathParam("numSerie")String numSerie) {
			return gestion.findlivreBynumSerie(Integer.parseInt(numSerie));
		}
	//find All
	@GET
	@Produces(MediaType.APPLICATION_JSON )
	@Path("all")
	public List<livre> getlivreList(){
		return gestion.findAlllivre();
	}

	//remove pathparam
	@DELETE
	@Path("D{numSerie}")
	public Response deletelivre(@PathParam("numSerie")String numSerie) {
		livre l= gestion.findlivreBynumSerie(Integer.parseInt(numSerie));
		gestion.deleteLivre(l);
		return Response.status(Status.OK).entity("Remove livre").build();
	}
	//Update livre
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response modifuser(livre livre) {
		gestion.updateLivre(livre);
		return Response.status(Status.OK).entity("update livre succesfull").build();
	}
}

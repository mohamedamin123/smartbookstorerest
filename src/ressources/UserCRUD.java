package ressources;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.Entity;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import smartbook.entities.Utilisateur;
import smartbook.service.GestionUtilisateurRemote;
@Entity
@Path("usercrud")
public class UserCRUD {
	@EJB
	private GestionUtilisateurRemote  gestion;
	ArrayList <Utilisateur> list =new  ArrayList<Utilisateur>();
	
	
	
	//find All
	@GET
	@Produces(MediaType.APPLICATION_JSON )
	@Path("all")
	public List<Utilisateur> getUtilisateurList(){
		return gestion.findAllUtilisateur();
	}
	//find By CIN response
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("{cin}")
	public Response getEtud(@PathParam("cin")String cin) {
		gestion.findUtilisateurByCin(Integer.parseInt(cin));
		return Response.status(Status.ACCEPTED).entity("par cin ").build();		
	}
	//remove pathparam
	@DELETE
	@Path("D{cin}")
	public Response deleteEtudp(@PathParam("cin")String cin) {
		Utilisateur u= gestion.findUtilisateurByCin(Integer.parseInt(cin));
		gestion.deleteEtudiant(u);
		return Response.status(Status.OK).entity("Remove").build();
	}
	//Update User
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response modifuser(Utilisateur u) {
		gestion.updateEtudiant(u);
		return Response.status(Status.OK).entity("update succesfull").build();
	}
}

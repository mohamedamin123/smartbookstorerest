package tn.iset.rest.ressources;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.Entity;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import edu.iset.smartbook.Utilisateur;
import edu.iset.smartbook.services.GestionUserRemote;

@Entity
@Path("Users")
public class RessourcesUser {
@EJB
private GestionUserRemote gestion;
ArrayList <Utilisateur> list =new  ArrayList<Utilisateur>();





//find All
@GET
@Produces(MediaType.APPLICATION_JSON )
@Path("all1")
public List<Utilisateur> getUserList(){
	return gestion.findAllUtilisateur();
}


//Update user
@PUT
@Produces({MediaType.APPLICATION_JSON})
public void updateUtilisateur(Utilisateur user) {
	gestion.updateUtilisateur(user);
}






//Ajout user
@POST
@Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON })
@Path(value="ajout1")
public Response Ajout(Utilisateur user) {
	gestion.addUser(user);
	return Response.status(Status.CREATED).entity("Ajout utilisateur en XML/JSON").build();
}
/*
//find All
@GET
@Produces(MediaType.APPLICATION_JSON )
@Path("all1")
public List<Utilisateur> getEtudiantList(){
	return gestion.findAllUtilisateur();
}

//authentifier form
@GET
@Consumes("application/x-www-form-urlencoded")
@Path("login")
public String loginf (@FormParam("email")String email,@FormParam("password")String password) {
	if (email.isEmpty()||password.isEmpty())
	{
		return "failed";
	}else
	{
		return gestion.login(email, password)+" login avec sucess form";
	}
}*/
} 